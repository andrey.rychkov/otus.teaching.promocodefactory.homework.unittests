﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Factory
{
    internal class RequestBuilder
    {
        private SetPartnerPromoCodeLimitRequest _request = null;

        internal RequestBuilder()
        {
            WithBase();
        }


        internal RequestBuilder WithBase()
        {
            _request = new SetPartnerPromoCodeLimitRequest()
            {
                Limit = 200,
                EndDate = DateTime.MaxValue
            };

            return this;
        }

        internal SetPartnerPromoCodeLimitRequest Build()
        {
            return _request;
        }

        internal RequestBuilder WhenLimitNegative()
        {
            _request.Limit = -1;

            return this;
        }
    }
}