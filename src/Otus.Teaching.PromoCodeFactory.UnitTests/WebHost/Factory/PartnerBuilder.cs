﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Factory
{
    internal class PartnerBuilder
    {

        private Partner _partner = null;

        internal PartnerBuilder()
        {
            WithBase();
        }

        internal PartnerBuilder WithEmpty()
        {
            _partner = new Partner();
            return this;
        }
        internal PartnerBuilder WithBase()
        {
            _partner = new Partner()
            {
                Id = Guid.Parse("7d994823-8226-4273-b063-1a95f3cc1df8"),
                Name = "Суперигрушки",
                IsActive = true,
                PartnerLimits = new List<PartnerPromoCodeLimit>()
                {
                    new PartnerPromoCodeLimit()
                    {
                        Id = Guid.Parse("e00633a5-978a-420e-a7d6-3e1dab116393"),
                        CreateDate = new DateTime(2020, 07, 9),
                        EndDate = new DateTime(2020, 10, 9),
                        Limit = 100
                    }
                }
            };

            return this;
        }
        internal Partner Build()
        {
            return _partner;
        }


        internal PartnerBuilder WithPartnerLimitsClear()
        {
            _partner.PartnerLimits.Clear();
            return this;
        }

        internal PartnerBuilder WithFalseActive()
        {
            _partner.IsActive = false;
            return this;
        }

        internal PartnerBuilder WithSingleIssuedPromoCode()
        {
            _partner.NumberIssuedPromoCodes = 1;
            return this;
        }

        internal PartnerBuilder WithCancelDateNull()
        {
            var partnerLimit = _partner.PartnerLimits.First();
            partnerLimit.CancelDate = null;
            return this;
        }

    }
}