﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Factory
{
    internal class FactorySetPartnerPromoCodeLimit
    {

        public PartnerBuilder GetPartnerBuilder()
        {
            return new PartnerBuilder();
        }
        public RequestBuilder GetRequestBuilder()
        {
            return new RequestBuilder();
        }

    }
}
