﻿using System;
using System.Collections.Generic;
using System.Linq;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromoCodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Controllers;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using Otus.Teaching.PromoCodeFactory.Core;
using Xunit;
using Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Factory;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.UnitTests.WebHost.Controllers.Partners
{
    public class SetPartnerPromoCodeLimitAsyncTests
    {
        private readonly Mock<IRepository<Partner>> _partnersRepositoryMock;
        private readonly PartnersController _partnersController;
        private readonly FactorySetPartnerPromoCodeLimit _fatory = new FactorySetPartnerPromoCodeLimit();

        public SetPartnerPromoCodeLimitAsyncTests()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _partnersRepositoryMock = fixture.Freeze<Mock<IRepository<Partner>>>();
            _partnersController = fixture.Build<PartnersController>().OmitAutoProperties().Create();
        }
  
        /// <summary>
        /// 1. Если партнер не найден, то также нужно выдать ошибку 404
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partner = _fatory.GetPartnerBuilder().Build();
            var request = _fatory.GetRequestBuilder().Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync((Partner)null);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            result.Should().BeAssignableTo<NotFoundResult>();
        }
        /// <summary>
        /// 2. Если партнер заблокирован, то есть поле IsActive = false в классе Partner, то также нужно выдать ошибку 400;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerIsNotActive_ReturnsBadRequest()
        {
            // Arrange
            var partner = _fatory.GetPartnerBuilder().WithFalseActive().Build();
            var request = _fatory.GetRequestBuilder().Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert
            
            var badRequest = result.Should().BeAssignableTo<BadRequestObjectResult>();
            Assert.Equal( "Данный партнер не активен", badRequest.Subject.Value);
        }

        /// <summary>
        /// 3.1 Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerSetLimit_ResultsResetPromocodes()
        {
            // Arrange
            var partner = _fatory.GetPartnerBuilder().Build();
            var request = _fatory.GetRequestBuilder().Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert

            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(0);

        }
        /// <summary>
        /// 3.2 Если партнеру выставляется лимит, то мы должны обнулить количество промокодов, 
        /// которые партнер выдал NumberIssuedPromoCodes, если лимит закончился, то количество не обнуляется;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerLimitEnded_ResultsNotResetPromocodes()
        {
            // Arrange
            var partner = _fatory.GetPartnerBuilder()
                .WithPartnerLimitsClear()
                .WithSingleIssuedPromoCode()
                .Build();
            var request = _fatory.GetRequestBuilder().Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert

            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.NumberIssuedPromoCodes.Should().Be(1);
        }

        /// <summary>
        /// 4. При установке лимита нужно отключить предыдущий лимит;
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_PartnerSetLimit_ResultsDisabledPreviousLimit()
        {
            // Arrange
            var partner = _fatory.GetPartnerBuilder().WithCancelDateNull().Build();
            var request = _fatory.GetRequestBuilder().Build();

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert

            result.Should().BeAssignableTo<CreatedAtActionResult>();
            partner.PartnerLimits.First().CancelDate.Should().NotBeNull();
            
        }

        /// <summary>
        /// 5. Лимит должен быть больше 0
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_LimitWrong_ResultsBadRequest()
        {
            // Arrange
            var partner = _fatory.GetPartnerBuilder().Build();
            var request = _fatory.GetRequestBuilder().WhenLimitNegative().Build();
       
            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert

            result.Should().BeAssignableTo<BadRequestObjectResult>();

        }

        /// <summary>
        /// 6. Нужно убедиться, что сохранили новый лимит в базу данных(это нужно проверить Unit-тестом);
        /// </summary>
        [Fact]
        public async Task SetPartnerPromoCodeLimitAsync_SetLimit_ResultsAddNewLimitExist()
        {
            // Arrange
            var partner = _fatory.GetPartnerBuilder().Build();
            var request = _fatory.GetRequestBuilder().Build();

            var oldPartnerCount = partner.PartnerLimits.Count;

            

            _partnersRepositoryMock.Setup(repo => repo.GetByIdAsync(partner.Id)).ReturnsAsync(partner);
 

            // Act
            var result = await _partnersController.SetPartnerPromoCodeLimitAsync(partner.Id, request);

            // Assert

            var newPartnerCount = partner.PartnerLimits.Count;
            result.Should().BeAssignableTo<CreatedAtActionResult>();

            oldPartnerCount.Should().BeLessThan(newPartnerCount);

        }

        

    }
}